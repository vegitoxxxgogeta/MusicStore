﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MusicStore.Business.Interfaces;
using MusicStore.Business.Requests;
using MusicStore.Business.Responses;
using MusicStore.Data.Data;
using MusicStore.Data.Interfaces;
using MusicStore.Data.Models;

namespace MusicStore.Business.Services 
{
    public class ItemService : IItemService
    {
        private readonly IItemsRepository _itemsRepository;
        private readonly IItemTypeRepository _itemsTypeRepository;
        private ApplicationDbContext _applicationDbContext;
        public ItemService(IItemsRepository item, IItemTypeRepository itemsTypeRepository,  ApplicationDbContext applicationDbContext)
        {
            _itemsRepository = item;
            _itemsTypeRepository = itemsTypeRepository;
            _applicationDbContext = applicationDbContext;
        }
        public async Task<PagedResponse<Item[]>> GetAllItems(PagedRequest request)
        {
            var dbQuery = _itemsRepository.PagedItems();
            
            // searching
            var lowerQ = request.Search?.ToLower();
            if (!string.IsNullOrWhiteSpace(lowerQ))
            {
                dbQuery = (lowerQ?.Split(' ')).Aggregate(dbQuery, (current, searchWord) =>
                    current.Where(f =>
                        f.Description.ToLower().Contains(searchWord) ||
                        f.Name.ToLower().Contains(searchWord) ||
                        f.Price.ToString().ToLower().Contains(searchWord) || 
                        f.Type.Type.ToLower().Contains(searchWord) 
                    ));
            }

            // sorting
            if (!string.IsNullOrWhiteSpace(request.OrderColumn) && !string.IsNullOrWhiteSpace(request.OrderDir))
            {
                dbQuery = request.OrderColumn switch
                {
                    "description" => (request.OrderDir.Equals("asc"))
                        ? dbQuery.OrderBy(o => o.Description)
                        : dbQuery.OrderByDescending(o => o.Description),
                    "name" => (request.OrderDir.Equals("asc"))
                        ? dbQuery.OrderBy(o => o.Name)
                        : dbQuery.OrderByDescending(o => o.Name),
                    "price" => (request.OrderDir.Equals("asc"))
                        ? dbQuery.OrderBy(o => o.Price)
                        : dbQuery.OrderByDescending(o => o.Price),
                    "type" => (request.OrderDir.Equals("asc"))
                        ? dbQuery.OrderBy(o => o.Type.Type)
                        : dbQuery.OrderByDescending(o => o.Type.Type),
                    _ => (request.OrderDir.Equals("asc"))
                        ? dbQuery.OrderBy(o => o.Id)
                        : dbQuery.OrderByDescending(o => o.Id)
                };
            }
            
            // total count
            var totalItemCount = dbQuery.Count();

            // paging
            dbQuery = dbQuery.Skip((request.PageNumber - 1) * request.PageSize).Take(request.PageSize);
            
            var dbItems = await dbQuery.ToArrayAsync();
            
            return new PagedResponse<Item[]>(dbItems, totalItemCount, request.PageNumber, request.PageSize);
        }

        public IQueryable<Item> GetAllItems()
        {
            return _itemsRepository.GetBind();
        }
        public  IList<ItemType> GetAllTypes()
        {
            return _itemsTypeRepository.GetAll().ToList();
        }

        public async Task<List<Recall>> GetAllRecalls(int productId)
        {
            var recalls = await _applicationDbContext.Recalls.Include(x => x.Item).Include(x => x.User).Where(x => x.ItemFkId == productId).ToListAsync();
            return recalls;
        }
        public async Task CreateRecall(Recall recall)
        {
           await _applicationDbContext.Recalls.AddAsync(recall);
           await _applicationDbContext.SaveChangesAsync();
        }

        public async Task CreateItem(Item itemDto)
        {
            await _itemsRepository.Create(itemDto);
        }

        public async Task<Item> GetItem(int itemId)
        {
            return await _itemsRepository.Get(itemId);
        }

        public async Task UpdateItem(Item itemDto)
        {
            await _itemsRepository.Update(itemDto);
        }

        public async Task RemoveItem(int itemId)
        {
            await _itemsRepository.Remove(itemId);
        }
    }
}