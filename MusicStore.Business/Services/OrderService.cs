﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MusicStore.Business.Interfaces;
using MusicStore.Business.Service_Models;
using MusicStore.Data.Interfaces;
using MusicStore.Data.Models;

namespace MusicStore.Business.Services
{
    public class OrderService : IOrderService
    {
        private readonly IOrderRepository _order;

        public OrderService(IOrderRepository order)
        {
            _order = order;
        }

        public IQueryable<UsersOrders> GetUsersOrders(string userId)
        {
            return _order.GetUsersOrders(userId);
        }

        public Guid GetUniqueOrderId(int orderId)
        {
            return _order.GetOrderId(orderId);
        }

        public IEnumerable<Order> GetOrderDetails(Guid orderId)
        {
            return _order.OrderDetails(orderId);
        }
        //This method allows to get all anonymous orders and filters list of themselves by sorting and searching
        public IQueryable<AnonymousOrders> FiltrateAnonymousOrders(string sortOrder, string searchString, OrderFilterType type)
        {
            IQueryable<AnonymousOrders> orders = type == OrderFilterType.Logs ? _order.GetLogsAnonymous() : _order.GetUnproccessedAnonymous();
            
            if (!String.IsNullOrEmpty(searchString))
            {
                orders = orders.Where(s => s.Date.ToString().Contains(searchString)
                                           || s.OrderId.ToString().Contains(searchString)
                                           || s.Email.Contains(searchString)
                                           || s.Id.ToString().Contains(searchString));
                
            }

            orders = sortOrder switch
            {
                "IdDesc" => orders.OrderByDescending(s => s.Id),
                "Id" => orders.OrderBy(s => s.Id),
                "EmailDesc" => orders.OrderByDescending(s => s.Email),
                "Email" => orders.OrderBy(s => s.Email),
                "OrderIdDesc" => orders.OrderByDescending(s => s.OrderId),
                "OrderId" => orders.OrderBy(s => s.OrderId),
                "StatusDesc" => orders.OrderByDescending(s => s.Status),
                "Status" => orders.OrderBy(s => s.Status),
                "DateDesc" => orders.OrderByDescending(s => s.Date),
                "Date" => orders.OrderBy(s => s.Date),
                _ => orders.OrderBy(s => s.Date)
            };

            return orders;
        }
        
        //This method allows to get all authorized orders and filters list of themselves by sorting and searching
        public IQueryable<UsersOrders> FiltrateUsersOrders(string sortOrder, string searchString, OrderFilterType type)
        {
            var orders =   type == OrderFilterType.Unproccessed ? _order.GetUnproccessed() : _order.GetLogs();
            if (!String.IsNullOrEmpty(searchString))
            {
                orders = orders.Where(s => s.Date.ToString().Contains(searchString)
                                           || s.OrderId.ToString().Contains(searchString)
                                           || s.User.Email.Contains(searchString)
                                           || s.Id.ToString().Contains(searchString));
                
            }

            orders = sortOrder switch
            {
                "IdDesc" => orders.OrderByDescending(s => s.Id),
                "Id" => orders.OrderBy(s => s.Id),
                "UserIdDesc" => orders.OrderByDescending(s => s.UserId),
                "UserId" => orders.OrderBy(s => s.UserId),
                "OrderIdDesc" => orders.OrderByDescending(s => s.OrderId),
                "OrderId" => orders.OrderBy(s => s.OrderId),
                "StatusDesc" => orders.OrderByDescending(s => s.Status),
                "Status" => orders.OrderBy(s => s.Status),
                "DateDesc" => orders.OrderByDescending(s => s.Date),
                "Date" => orders.OrderBy(s => s.Date),
                _ => orders.OrderBy(s => s.Date)
            };

            return orders;
        }

        public async Task<dynamic> GetFirstOrder(int itemId, OrderType type)
        {
            //If user is authorized than get authorized order 
            if (type == OrderType.Authorized)
            {
                var order = await _order.GetOrder(itemId);
                return order;
            }
            //otherwise get anonymous order
            else
            {
                var order = await _order.GetAnonymousOrder(itemId);
                return order;
            }
        }
        //changes status for anonymous order
        public async Task ChangeAnonymousOrderStatus(AnonymousOrders order, string email)
        {
            await _order.ChangeAnonymousOrderStatus(order);
            await SendEmail.Send(email, "Status change",
                $"Your order №{order.Id} has been changed to status {order.Status}");
        }
        //changes status for authorized order
        public async Task ChangeUsersOrderStatus(UsersOrders order, string email)
        {
            await _order.ChangeOrderStatus(order);
            await SendEmail.Send(email, "Status change",
                $"Your order №{order.Id} has been changed to status {order.Status}");
        }
    }
}