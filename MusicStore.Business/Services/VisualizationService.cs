﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MusicStore.Business.Interfaces;
using MusicStore.Data.Data;
using MusicStore.Data.Models;

namespace MusicStore.Business.Services
{
    public class VisualizationService : IVisualizationService
    {
        private readonly ApplicationDbContext _applicationDbContext;
        

        public VisualizationService(ApplicationDbContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
        }

        public IQueryable<Item> GetAllRecalls()
        {
            return _applicationDbContext.Items.Include(x => x.Recalls)
                .OrderByDescending(x => x.Recalls.Count)
                .Take(10)
                .AsNoTracking();
        }

        public async Task<int> RecallsCount(int productId)
        {
            return await _applicationDbContext.Recalls
                .Include(x => x.Item)
                .Where(x => x.ItemFkId == productId)
                .CountAsync();
        }
    }
}