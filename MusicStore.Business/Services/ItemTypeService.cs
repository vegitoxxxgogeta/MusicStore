﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MusicStore.Business.Interfaces;
using MusicStore.Business.Requests;
using MusicStore.Business.Responses;
using MusicStore.Data.Interfaces;
using MusicStore.Data.Models;

namespace MusicStore.Business.Services
{
    public class ItemTypeService : IItemTypeService
    {
        private readonly IItemTypeRepository _type;

        public ItemTypeService(IItemTypeRepository type)
        {
            _type = type;
        }

        public async Task<PagedResponse<ItemType[]>> GetAllTypes(PagedRequest request)
        {
            var dbQuery = _type.GetAll();
            
            // searching
            var lowerQ = request.Search?.ToLower();
            if (!string.IsNullOrWhiteSpace(lowerQ))
            {
                dbQuery = (lowerQ?.Split(' ')).Aggregate(dbQuery, (current, searchWord) =>
                    current.Where(f =>
                        f.Type.ToLower().Contains(searchWord)  ||
                        f.Id.ToString().ToLower().Contains(searchWord)
                    ));
            }

            // sorting
            if (!string.IsNullOrWhiteSpace(request.OrderColumn) && !string.IsNullOrWhiteSpace(request.OrderDir))
            {
                dbQuery = request.OrderColumn switch
                {
                    "type" => (request.OrderDir.Equals("asc"))
                        ? dbQuery.OrderBy(o => o.Type)
                        : dbQuery.OrderByDescending(o => o.Type),
                    _ => (request.OrderDir.Equals("asc"))
                        ? dbQuery.OrderBy(o => o.Id)
                        : dbQuery.OrderByDescending(o => o.Id)
                };
            }
            
            // total count
            var totalItemCount = dbQuery.Count();

            // paging
            dbQuery = dbQuery.Skip((request.PageNumber - 1) * request.PageSize).Take(request.PageSize);
            
            var dbItems = await dbQuery.ToArrayAsync();
            
            return new PagedResponse<ItemType[]>(dbItems, totalItemCount, request.PageNumber, request.PageSize);
        }

        public async Task CreateType(ItemType typeDto)
        {
            await _type.Create(typeDto);
        }
        
        public async Task<ItemType> GetType(int typeId)
        {
            return await _type.Get(typeId);
        }
        
        public async Task UpdateType(ItemType typeDto)
        {
            await _type.Update(typeDto);
        }

        public async Task RemoveType(int typeId)
        {
           await _type.Remove(typeId);
        }
        
    }
}