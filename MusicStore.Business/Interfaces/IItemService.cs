﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MusicStore.Business.Requests;
using MusicStore.Business.Responses;
using MusicStore.Data.Models;

namespace MusicStore.Business.Interfaces
{
    public interface IItemService
    {
        Task<PagedResponse<Item[]>> GetAllItems(PagedRequest request);
        IQueryable<Item> GetAllItems();
        IList<ItemType> GetAllTypes();
        Task CreateItem(Item itemDto);
        Task<Item> GetItem(int itemId);
        Task UpdateItem(Item itemDto);
        Task RemoveItem(int itemId);
        Task<List<Recall>> GetAllRecalls(int productId);
        Task CreateRecall(Recall recall);
    }
}