﻿using System.Linq;
using System.Threading.Tasks;
using MusicStore.Business.Requests;
using MusicStore.Business.Responses;
using MusicStore.Data.Models;

namespace MusicStore.Business.Interfaces
{
    public interface IItemTypeService
    {
        Task<PagedResponse<ItemType[]>> GetAllTypes(PagedRequest request);
        Task CreateType(ItemType typeDto);
        Task<ItemType> GetType(int typeId);
        Task UpdateType(ItemType typeDto);
        Task RemoveType(int typeId);
    }
}