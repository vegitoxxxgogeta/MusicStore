﻿using System.Linq;
using System.Threading.Tasks;
using MusicStore.Data.Models;

namespace MusicStore.Business.Interfaces
{
    public interface IVisualizationService
    {
        IQueryable<Item> GetAllRecalls();
        Task<int> RecallsCount(int productId);
    }
}