﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MusicStore.Data.Migrations
{
    public partial class RecallModel2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "UserId",
                table: "Recalls",
                type: "text",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Recalls_UserId",
                table: "Recalls",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Recalls_AspNetUsers_UserId",
                table: "Recalls",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Recalls_AspNetUsers_UserId",
                table: "Recalls");

            migrationBuilder.DropIndex(
                name: "IX_Recalls_UserId",
                table: "Recalls");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "Recalls");
        }
    }
}
