﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MusicStore.Data.Migrations
{
    public partial class RecallCascade3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Recalls_AspNetUsers_UserId",
                table: "Recalls");

            migrationBuilder.AddForeignKey(
                name: "FK_Recalls_AspNetUsers_UserId",
                table: "Recalls",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Recalls_AspNetUsers_UserId",
                table: "Recalls");

            migrationBuilder.AddForeignKey(
                name: "FK_Recalls_AspNetUsers_UserId",
                table: "Recalls",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
