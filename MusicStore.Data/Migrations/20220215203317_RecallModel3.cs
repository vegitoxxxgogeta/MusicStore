﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MusicStore.Data.Migrations
{
    public partial class RecallModel3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Recalls_Items_ItemId",
                table: "Recalls");

            migrationBuilder.DropIndex(
                name: "IX_Recalls_ItemId",
                table: "Recalls");

            migrationBuilder.DropColumn(
                name: "ItemId",
                table: "Recalls");

            migrationBuilder.CreateIndex(
                name: "IX_Recalls_ItemFkId",
                table: "Recalls",
                column: "ItemFkId");

            migrationBuilder.AddForeignKey(
                name: "FK_Recalls_Items_ItemFkId",
                table: "Recalls",
                column: "ItemFkId",
                principalTable: "Items",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Recalls_Items_ItemFkId",
                table: "Recalls");

            migrationBuilder.DropIndex(
                name: "IX_Recalls_ItemFkId",
                table: "Recalls");

            migrationBuilder.AddColumn<int>(
                name: "ItemId",
                table: "Recalls",
                type: "integer",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Recalls_ItemId",
                table: "Recalls",
                column: "ItemId");

            migrationBuilder.AddForeignKey(
                name: "FK_Recalls_Items_ItemId",
                table: "Recalls",
                column: "ItemId",
                principalTable: "Items",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
