﻿using MusicStore.Data.Models;

namespace MusicStore.Data.Interfaces
{
    public interface IItemTypeRepository : IRepository<ItemType>
    {
        
    }
}