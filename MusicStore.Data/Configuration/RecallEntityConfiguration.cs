﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MusicStore.Data.Models;

namespace MusicStore.Data.Configuration
{
    public class RecallEntityConfiguration : IEntityTypeConfiguration<Recall>
    {
        public void Configure(EntityTypeBuilder<Recall> builder)
        {
            builder.HasKey(x => x.Id);

            builder.HasOne(x => x.Item)
                .WithMany(x => x.Recalls)
                .HasForeignKey(x => x.ItemFkId)
                .OnDelete(DeleteBehavior.Cascade);
            
            builder.HasOne(x => x.User)
                .WithMany(x => x.Recalls)
                .HasForeignKey(x => x.UserId)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}