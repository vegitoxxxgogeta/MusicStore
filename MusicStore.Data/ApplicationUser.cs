﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;
using MusicStore.Data.Models;

namespace MusicStore.Data
{
    public class ApplicationUser : IdentityUser
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Address { get; set; }
        
        public ICollection<Recall> Recalls { get; set; }
        
    }
}