﻿

using System.Collections;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;

namespace MusicStore.Data.Models
{
    public class Item
    {

        public int Id { get; set; }
        public string  Name { get; set; }
        public int  Price { get; set; }
        public string Description { get; set; }
        public int TypeId { get; set; }
        public ItemType Type { get; set; }
        public string ImageName { get; set; }
        public IFormFile ImageFile { get; set; }
        
        public ICollection<Recall> Recalls { get; set; }
    }
}