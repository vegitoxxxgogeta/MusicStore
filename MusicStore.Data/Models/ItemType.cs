﻿

namespace MusicStore.Data.Models
{
    public class ItemType
    {
        public int Id { get; set; }
        public string Type { get; set; }
    }
}