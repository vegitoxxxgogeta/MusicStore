﻿using System;

namespace MusicStore.Data.Models
{
    public class Recall
    {
        public int Id { get; set; }
        
        public string Text { get; set; }
        public int Rate { get; set; }
        public int ItemFkId { get; set; }
        public Item Item { get; set; }
        public string UserId { get; set; }
        public ApplicationUser User { get; set; }
        public DateTime Date { get; set; }
    }
}