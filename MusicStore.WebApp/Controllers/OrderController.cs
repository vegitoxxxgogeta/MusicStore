﻿﻿using System;
 using System.IO;
 using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
 using Microsoft.AspNetCore.Hosting;
 using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using MusicStore.Business.Interfaces;
using MusicStore.Business.Service_Models;
using MusicStore.Data;
using MusicStore.Data.Models;
using MusicStore.WebApp.Models;
using X.PagedList;
 using Xceed.Document.NET;
 using Xceed.Words.NET;

 namespace MusicStore.WebApp.Controllers
{
    public class OrderController : Controller
    {
        private readonly IOrderService _orderService;
        private readonly IWebHostEnvironment _webHostEnvironment;
        private readonly UserManager<ApplicationUser> _userManager;

        public OrderController(UserManager<ApplicationUser> userManager, IOrderService orderService, IWebHostEnvironment webHostEnvironment)
        {
            _userManager = userManager;
            _orderService = orderService;
            _webHostEnvironment = webHostEnvironment;
        }
    
        [HttpGet]
        [Authorize]
        public async Task<IActionResult> GetOrders(int pageNumber=1)
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            var orders = _orderService.GetUsersOrders(userId);
            var pagedList = new IndexViewModel();
            pagedList.UsersOrders = await PaginatedList<UsersOrders>.CreateAsync(orders, pageNumber, 8);
            return View("MyOrders",pagedList);

        }
        [Authorize]
        [HttpGet]
        public IActionResult GetDetails(int id)
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            var userOrder = _orderService.GetUsersOrders(userId).FirstOrDefault(i => i.OrderId == id);
            //get unique order id key for getting other items of user's order
            var orderIdKey = _orderService.GetUniqueOrderId(userOrder.OrderId);
            var items = _orderService.GetOrderDetails(orderIdKey);
            return PartialView("Details", new OrderViewModel
            {
                Id = id,
                Orders = items
            });
        }
        
        [Authorize]
        [HttpGet]
        public IActionResult GetCheck([FromQuery] int orderId)
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            var userOrder = _orderService.GetUsersOrders(userId).FirstOrDefault(i => i.OrderId == orderId);
            //get unique order id key for getting other items of user's order
            var orderIdKey = _orderService.GetUniqueOrderId(userOrder.OrderId);
            var items = _orderService.GetOrderDetails(orderIdKey).ToArray();
            
            var path = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            var fileName = "Check.docx";
            var createPathFile = Path.Combine(path, fileName);
            var doc = DocX.Create(createPathFile);
            decimal? totalPrice = 0;
            doc.InsertParagraph("Товарный чек").Font(new Font("Times New Roman")).FontSize(24).Bold().Alignment = Alignment.center;
            doc.InsertParagraph().SpacingAfter(1.5);
            doc.InsertParagraph("Наименование организации: ").Bold().Font(new Font("Times New Roman")).FontSize(14)
                .Append("Music Store").Font(new Font("Times New Roman"))
                .FontSize(14).IndentationFirstLine = 42f;

            doc.InsertParagraph("ФИО: ").Bold().Font(new Font("Times New Roman")).FontSize(14)
                .Append($"{userOrder.User.Name} {userOrder.User.Surname}").Font(new Font("Times New Roman"))
                .FontSize(14).IndentationFirstLine = 42f;
            
            doc.InsertParagraph("Email: ").Bold().Font(new Font("Times New Roman")).FontSize(14)
                .Append($"{userOrder.User.Email}").Font(new Font("Times New Roman"))
                .FontSize(14).IndentationFirstLine = 42f;
            
            doc.InsertParagraph("Адрес: ").Bold().Font(new Font("Times New Roman")).FontSize(14)
                .Append("Ул.Горького 12").Font(new Font("Times New Roman"))
                .FontSize(14).IndentationFirstLine = 42f;
            
            doc.InsertParagraph().SpacingAfter(1.5);

            doc.InsertParagraph($"Товарный чек №{orderId} от {userOrder.Date:d}").Bold()
                .Font(new Font("Times New Roman"))
                .FontSize(14).Alignment = Alignment.center;

            var table = doc.AddTable(items.Length + 1, 3);
            
            table.Design = TableDesign.MediumShading1Accent1;
            table.Alignment = Alignment.center;
            
            table.Rows[ 0 ].Cells[ 0 ].Paragraphs[ 0 ].Append("Продукт").Font(new Font("Times New Roman")).FontSize(14);
            table.Rows[ 0 ].Cells[ 1 ].Paragraphs[ 0 ].Append("Цена").Font(new Font("Times New Roman")).FontSize(14);
            table.Rows[ 0 ].Cells[ 2 ].Paragraphs[ 0 ].Append("Количество").Font(new Font("Times New Roman")).FontSize(14);
            
            for (var i = 0; i < items.Length; i++)
            {
                var cell = 0;
                table.Rows[ i + 1].Cells[ cell ].Paragraphs[ 0 ].Append(items[i].Item.Name).Font(new Font("Times New Roman")).FontSize(14);
                table.Rows[ i + 1].Cells[ cell + 1 ].Paragraphs[ 0 ].Append(items[i].Item.Price.ToString()).Font(new Font("Times New Roman")).FontSize(14);
                table.Rows[ i + 1].Cells[ cell + 2 ].Paragraphs[ 0 ].Append(items[i].Count.ToString()).Font(new Font("Times New Roman")).FontSize(14);

                totalPrice += items[i].Price.Price * items[i].Count;
            }
            
            var tableParagraph = doc.InsertParagraph().SpacingAfter(1.5);
            
            tableParagraph.InsertTableAfterSelf(table);
            
            doc.InsertParagraph().SpacingAfter(1);
            
            doc.InsertParagraph($"Итого: ").Font(new Font("Times New Roman")).FontSize(14)
                .Append($"{items.Length} ").Bold().Font(new Font("Times New Roman")).FontSize(14)
                .Append($"товар(а) на сумму ").Font(new Font("Times New Roman")).FontSize(14)
                .Append($"{totalPrice} ").Bold().Font(new Font("Times New Roman")).FontSize(14)
                .Append($"рублей").Font(new Font("Times New Roman")).FontSize(14);
            
            doc.InsertParagraph().SpacingAfter(0.5).SpacingBefore(0.5);
            
            doc.InsertParagraph($"Покупатель: ").Font(new Font("Times New Roman")).FontSize(14).Append($"{userOrder.User.Surname} {userOrder.User.Name}").Font(new Font("Times New Roman")).FontSize(14)
                .UnderlineStyle(UnderlineStyle.thick).Font(new Font("Times New Roman")).FontSize(14)
                .Append("      Подпись: ").Font(new Font("Times New Roman")).FontSize(14)
                .UnderlineStyle(UnderlineStyle.none).Append("__________").Font(new Font("Times New Roman")).FontSize(14).UnderlineStyle(UnderlineStyle.thick);
            
            doc.Save();
            return PhysicalFile(createPathFile, "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
                fileName);
        }
        
        [Authorize(Roles = "Admin")]
        [HttpGet]
        public IActionResult GetUnproccessedAnonymous(string sortOrder,string searchString,string currentFilter, int? page)
        {
            
            ViewBag.CurrentSort = sortOrder;
            ViewBag.Date = String.IsNullOrEmpty(sortOrder) ? "date" : "";
            ViewBag.Status = sortOrder == "Status" ? "StatusDesc" : "Status";
            ViewBag.OrderId = sortOrder == "OrderId"? "OrderIdDesc" : "OrderId";
            ViewBag.Email = sortOrder == "Email" ? "EmailDesc" : "Email";
            ViewBag.Id = sortOrder == "Id" ? "IdDesc" : "Id";
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewBag.CurrentFilter = searchString;
            var orders = _orderService.FiltrateAnonymousOrders( sortOrder, searchString, OrderFilterType.Unproccessed);
            int pageSize = 8;
            int pageNumber = (page ?? 1);
            var pagedList = new IndexViewModel();
            pagedList.BootstrapAnonymousOrders = orders.ToPagedList(pageNumber, pageSize);
            return View("UnproccessedAnonymous",pagedList);
        }
        [Authorize(Roles = "Admin")]
        [HttpGet]
        public IActionResult GetUnproccessed(string sortOrder,string searchString,string currentFilter, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.Date = sortOrder == "Date" ? "DateDesc" : "Date";
            ViewBag.Status = sortOrder == "Status" ? "StatusDesc" : "Status";
            ViewBag.OrderId = sortOrder == "OrderId"? "OrderIdDesc" : "OrderId";
            ViewBag.UserId = sortOrder == "UserId" ? "UserIdDesc" : "UserId";
            ViewBag.Id = sortOrder == "Id" ? "IdDesc" : "Id";
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            
            ViewBag.CurrentFilter = searchString;
            var orders = _orderService.FiltrateUsersOrders(sortOrder, searchString, OrderFilterType.Unproccessed);
            int pageSize = 8;
            int pageNumber = (page ?? 1);
            var pagedList = new IndexViewModel();
            pagedList.BootstrapUsersOrders = orders.ToPagedList(pageNumber, pageSize);
            return View("Unproccessed",pagedList);
        }
        [Authorize(Roles = "Admin")]
        [HttpGet]
        public async Task<IActionResult> Finished(string sortOrder,string searchString,string currentFilter, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.Date = sortOrder == "Date" ? "DateDesc" : "Date";
            ViewBag.Status = sortOrder == "Status" ? "StatusDesc" : "Status";
            ViewBag.OrderId = sortOrder == "OrderId"? "OrderIdDesc" : "OrderId";
            ViewBag.UserId = sortOrder == "UserId" ? "UserIdDesc" : "UserId";
            ViewBag.Id = sortOrder == "Id" ? "IdDesc" : "Id";
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            
            ViewBag.CurrentFilter = searchString;
            var orders = _orderService.FiltrateUsersOrders(sortOrder, searchString, OrderFilterType.Logs);
            int pageNumber = (page ?? 1);
            var pagedList = new IndexViewModel();
            pagedList.UsersOrders = await PaginatedList<UsersOrders>.CreateAsync(orders, pageNumber, 5);
            return View("Finished",pagedList);
        }
         [Authorize(Roles = "Admin")]
        [HttpGet]
        public async Task<IActionResult> FinishedAnonymous(string sortOrder,string searchString,string currentFilter, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.Date = sortOrder == "Date" ? "DateDesc" : "Date";
            ViewBag.Status = sortOrder == "Status" ? "StatusDesc" : "Status";
            ViewBag.OrderId = sortOrder == "OrderId"? "OrderIdDesc" : "OrderId";
            ViewBag.Email = sortOrder == "Email" ? "EmailDesc" : "Email";
            ViewBag.Id = sortOrder == "Id" ? "IdDesc" : "Id";
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewBag.CurrentFilter = searchString;
            var orders = _orderService.FiltrateAnonymousOrders(sortOrder, searchString, OrderFilterType.Logs);
            int pageNumber = (page ?? 1);
            var pagedList = new IndexViewModel();
            pagedList.AnonymousOrders = await PaginatedList<AnonymousOrders>.CreateAsync(orders, pageNumber, 5);
            return View("FinishedAnonymous",pagedList);
        }
        [HttpGet]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> ChangeStatus(int itemId, OrderType type)
        {
            if (type == OrderType.Authorized)
            {
                var order = await _orderService.GetFirstOrder(itemId, type);
                var orderView = new OrderViewModel()
                {
                    Id = itemId,
                    Status = order.Status,
                    UserId = order.UserId,
                    OrderId = order.OrderId
                };
                return View("Status", orderView); 
            }
            else
            {
                var order = await _orderService.GetFirstOrder(itemId, type);
                var orderView = new OrderViewModel()
                {
                    Id = itemId,
                    Status = order.Status,
                    Email = order.Email,
                    OrderId = order.OrderId
                };
                return View("Status", orderView); 
            }
        }
        
        [HttpPost]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> ChangeStatus(OrderViewModel orderDto)
        {
            //if user is authorized
            if (orderDto.Type == OrderType.Authorized)
            {
                //than create authorized order
                var order = new UsersOrders()
                {
                    Id = orderDto.Id,
                    Status = orderDto.Status,
                    UserId = orderDto.UserId
                };
                var user = await _userManager.FindByIdAsync(order.UserId);
                var email = user.Email;
                await _orderService.ChangeUsersOrderStatus(order, email);
                return Redirect("/Order/GetUnproccessed");
            }
            //otherwise create anonymous order
            else
            {
                var order = new AnonymousOrders()
                {
                    Id = orderDto.Id,
                    Status = orderDto.Status,
                    Email = orderDto.Email
                };
                var email = order.Email;
                await _orderService.ChangeAnonymousOrderStatus(order, email);
               
                return Redirect("/Order/GetUnproccessedAnonymous");
            }
        }
        }
   
    }
