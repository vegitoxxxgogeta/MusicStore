﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Localization;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Localization;
using MusicStore.Business.Interfaces;
using MusicStore.Data.Models;
using MusicStore.WebApp.Helpers;
using MusicStore.WebApp.Models;
using X.PagedList;

namespace MusicStore.WebApp.Controllers
{
    public class CartController : Controller
    {
    
        private readonly ICartService _cartService;
        private readonly IItemService _itemService;
        private readonly IStringLocalizer<CartController> Localizer;
        private readonly IStringLocalizerFactory _localizerFactory;
        public CartController(ICartService cartService, IItemService itemService, IStringLocalizer<CartController> localizer, IStringLocalizerFactory localizerFactory)
        {
            _cartService = cartService;
            _itemService = itemService;
            Localizer = localizer;
            _localizerFactory = localizerFactory;
        }
        [AllowAnonymous]
        [HttpGet]
         public async Task<IActionResult> Items(string type, string sortName, string searchString,string currentFilter, int? page)
         {
             if (type == "0")
                 type = null;
             ViewBag.CurrentFilter = searchString;
             ViewBag.Type = type;
             ViewBag.PriceSorting = sortName;
             var homeModel = await _cartService.GetItems(type,  searchString, sortName);
             var priceSortList = new List<PriceSorter>();
             priceSortList.Add(new PriceSorter()
             {
                 Value = 1,
                 SortName = Localizer.GetString("Ascending price").Value
             });
             
             priceSortList.Add(new PriceSorter()
             {
                 Value = 2,
                 SortName = Localizer.GetString("Descending price").Value
             });
             
            ViewBag.Types = homeModel.Types.Prepend(new ItemType()
            {
                Id = 0,
                Type = Localizer.GetString("All").Value
            });
            ViewBag.PriceSort = priceSortList;
            
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }
           

            int pageSize = 8;
            int pageNumber = (page ?? 1);
            var pagedList = new IndexViewModel();
            pagedList.Items = homeModel.Items.ToPagedList(pageNumber, pageSize);
            return View(pagedList);
            
        }
         [HttpGet]
         [AllowAnonymous]
         public async Task<IActionResult> AddToCart(int id)
         {
                 var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
                 if (userId == null)
                 {
                     var item = await _itemService.GetItem(id);
                     if (Session.GetObjectFromJson<List<Cart>>(HttpContext.Session, "cart") == null)
                     {
                         List<Cart> cart = new List<Cart>();
                         _cartService.AddToAnonymousCart(cart, item, id);
                         Session.SetObjectAsJson(HttpContext.Session, "cart", cart);
                     }
                     else
                     {
                         List<Cart> cart = Session.GetObjectFromJson<List<Cart>>(HttpContext.Session, "cart");
                         int ind = _cartService.CheckItemInCart(cart, id);
                         if (ind != -1)
                         {
                             return BadRequest();
                         }
                         else
                         {
                             _cartService.AddToAnonymousCart(cart, item, id);
                         }
                         Session.SetObjectAsJson(HttpContext.Session, "cart", cart);
                     }
                 }
                 else
                 {
                     var usersCart = await PagedListExtensions.ToListAsync(_cartService.GetUsersCart(userId));
                     var index = usersCart.FindIndex(item => item.ItemId == id);
                     if (index >= 0)
                     {
                         return BadRequest();
                     }

                     await _cartService.AddToCart(id, userId);
                     return Ok();
                 }
                 return Ok();
         }
         
         [AllowAnonymous]
         [HttpGet]
         public async Task<ActionResult> GetCart()
        {
            var pagedList = new IndexViewModel();
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            if (userId == null)
            {
                var sessionCart = Session.GetObjectFromJson<List<Cart>>(HttpContext.Session, "cart");
                pagedList.Cart = sessionCart;
               
                if (sessionCart != null)
                {
                    pagedList.ItemsQuantities = _cartService.SetQuantitiesForAnonymous(sessionCart);
                    return View("Cart", pagedList);
                }
                pagedList.Cart = await PagedListExtensions.ToListAsync(_cartService.GetUsersCart("0"));
                return View("Cart", pagedList);
            }
            var cart = _cartService.GetUsersCart(userId);
            pagedList.Cart = await PagedListExtensions.ToListAsync(cart);
            pagedList.ItemsQuantities = _cartService.SetQuantitiesForUser(cart);
            return View("Cart", pagedList);
        }
         
       
         
         [HttpPost]
         [AllowAnonymous]
         public async Task<IActionResult> AnonymousSubmit(AnonymousViewModel model)
         {
             if (ModelState.IsValid)
             {
                 List<Order> orders = Session.GetObjectFromJson<List<Order>>(HttpContext.Session, "orders");
                 await _cartService.SubmitForAnonymous(orders, model.Email, model.Address);
                 HttpContext.Session.Clear();
                 return View("OrderInfo", model);
             }
             return View("AnonymousSubmit");
         }
         
         [HttpGet]
         [AllowAnonymous]
         public async Task<IActionResult> AddOrder(IndexViewModel model)
         {
            
             if (ModelState.IsValid)
             {
                 var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
                 if (userId == null)
                 {
                     List<Cart> cart = Session.GetObjectFromJson<List<Cart>>(HttpContext.Session, "cart");
                     List<Order> orders = _cartService.AddCookieOrder(cart);
                     for (int i = 0; i < orders.Count(); i++)
                         orders[i].Count = model.ItemsQuantities[i];
                     Session.SetObjectAsJson(HttpContext.Session, "orders", orders);
                     return View("AnonymousSubmit");
                 }
                 var userEmail = User.FindFirstValue(ClaimTypes.Email);
                 var usersCart = await _cartService.GetUsersCart(userId).ToListAsync();
                 var itemsQuantities = model.ItemsQuantities;
                 var list = _cartService.AddCookieOrder(usersCart);
                 
                 for (int i = 0; i < list.Count(); i++)
                     list[i].Count = itemsQuantities[i];

                 await _cartService.SubmitOrder(list, userEmail, userId);
                 return Redirect("/Order/GetOrders");
             }
             return Redirect("/Item/Items");
         }
            
         [AllowAnonymous]
         [HttpGet]
         public async Task<IActionResult> Details(int id)
         {
             var cart = _itemService.GetAllItems().FirstOrDefault(com => com.Id == id);
             var recalls = await _itemService.GetAllRecalls(id);
             var recallList = new List<RecallViewModel>();
             recallList.AddRange(recalls.Select(x => new RecallViewModel()
             {
                 Rate = x.Rate,
                 Text = x.Text,
                 ItemFkId = x.ItemFkId,
                 Date = x.Date,
                 User = x.User.Name + " " + x.User.Surname
             }));
             
             var itemViewModel = new ItemDetailsViewModel()
             {
                 Item = new ItemViewModel()
                 {
                     Id = cart.Id,
                     Name = cart.Name,
                     Price = cart.Price,
                     Description = cart.Description,
                     ImageName = cart.ImageName,
                     TypeId = cart.TypeId
                 },
                Recall = recallList
                
             };
             return PartialView(itemViewModel);
            
         }

         [Authorize]
         [HttpPost]
         public async Task CreateRecall([FromBody] RecallViewModel model)
         {
             var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
             var recall = new Recall()
             {
                 Date = DateTime.Now,
                 Rate = 2,
                 Text = model.Text,
                 UserId = userId,
                 ItemFkId = model.ItemFkId
             };

             await _itemService.CreateRecall(recall);
         }

         [AllowAnonymous]
         [HttpGet]
         public async Task<IActionResult> GetItemsCount()
         {
             var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
             if (userId != null) return Ok(await _cartService.GetCount(userId));
             List<Cart> cart = Session.GetObjectFromJson<List<Cart>>(HttpContext.Session, "cart");
             return Ok(cart.Count());
         }
         

         [HttpGet]
         [AllowAnonymous]
         public async Task<IActionResult> RemoveCartItem(int cartId)
         { 
             var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
             if (userId == null)
             {
                 List<Cart> cart = Session.GetObjectFromJson<List<Cart>>(HttpContext.Session, "cart");
                 int index = _cartService.CheckCartValue(cart, cartId);
                 cart.RemoveAt(index);
                 Session.SetObjectAsJson(HttpContext.Session, "cart", cart);
                 return Redirect("/Cart/GetCart");
             }
             else
             {
                 await _cartService.RemoveCartItem(cartId);
                 return Redirect("/Cart/GetCart");
             }
         }
    }
}