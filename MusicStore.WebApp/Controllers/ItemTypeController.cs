﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MusicStore.Business.Interfaces;
using MusicStore.Data.Models;
using MusicStore.WebApp.Models;


namespace MusicStore.WebApp.Controllers
{
    public class ItemTypeController : Controller
    {
        private readonly IItemTypeService _typeService;

        public ItemTypeController(IItemTypeService typeService)
        {
            _typeService = typeService;
        }
        [HttpGet("[controller]")]
        public IActionResult Types()
        {
           return View();
        }
        
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<ActionResult> Types([FromBody]DtQuery query)
        {
            var types =  await _typeService.GetAllTypes(query);
            return Json(types);
        }
        
        [Authorize(Roles = "Admin")]
        public  IActionResult AddType()
        {
            return View();
        }
        [HttpPost]
        [Authorize(Roles = "Admin")]
       
        public async Task<IActionResult> AddTypePost(ItemTypeViewModel typeDto)
        {
            if (!ModelState.IsValid) return View("AddType");
            var type = new ItemType()
            {
                Type = typeDto.Type
            };
            await _typeService.CreateType(type);
            return Redirect("/ItemType/");
        }
        
        [HttpGet("[controller]/{typeId}/[action]")]   
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> EditType([FromRoute] int typeId)
        {
            var type = await _typeService.GetType(typeId);
            var itemView = new ItemTypeViewModel()
            {
                Id = type.Id,
                Type = type.Type
            };
            return View(itemView);
        }
        
        [HttpPost("")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> EditTypePost(ItemTypeViewModel typeDto)
        {
            if (ModelState.IsValid)
            {
                var type = new ItemType()
                {
                    Id = typeDto.Id,
                    Type = typeDto.Type
                };
                await _typeService.UpdateType(type);
                return Redirect("/ItemType/");
            }
            ViewBag.Message = string.Format("Input error!");
            return View("EditType");
        }
        [HttpDelete("[controller]/{typeId}")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> DeleteType([FromRoute]int typeId)
        {
            await _typeService.RemoveType(typeId);
            return Ok();
        }
    }
}