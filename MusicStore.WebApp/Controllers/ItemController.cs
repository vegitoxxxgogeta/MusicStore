﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ClosedXML.Excel;
using DocumentFormat.OpenXml.Spreadsheet;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using MusicStore.Business.Interfaces;
using MusicStore.WebApp.Helpers;
using MusicStore.WebApp.Models;
using X.PagedList;
using Item = MusicStore.Data.Models.Item;


namespace MusicStore.WebApp.Controllers
{
    [Authorize(Roles = "Admin")]
    public class ItemController : Controller
    {
        private readonly IItemService _itemService;
        private readonly IWebHostEnvironment _webHostEnvironment;
   

        public ItemController(IItemService itemService,  IWebHostEnvironment webHostEnvironment)
        {
            _webHostEnvironment = webHostEnvironment;
            _itemService = itemService;
        }
        [HttpGet]
        public IActionResult Items()
        {
            return View();
        }

        [HttpGet]
        public IActionResult GetExcel()
        {
            var products = _itemService.GetAllItems();
            var productList = new List<ItemViewModel>();
            productList.AddRange(products.Select(x => new ItemViewModel()
            {
                Name = x.Name,
                Price = x.Price,
                Description = x.Description
            }));

            using var workbook = new XLWorkbook(XLEventTracking.Disabled);
            var worksheet = workbook.Worksheets.Add("Products");

            worksheet.Cell("A1").Value = "Название";
            worksheet.Cell("B1").Value = "Цена";

            worksheet.Range("A1:B1").Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
            worksheet.Row(1).Style.Font.Bold = true;

            for (var i = 0; i < productList.Count; i++)
            {
                worksheet.Range($"A{i + 2}:B{i + 2}").Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(i + 2, 1).Value = productList[i].Name;
                worksheet.Cell(i + 2, 2).Value = productList[i].Price;

            }

            worksheet.ColumnWidth = 30;
            using var stream = new MemoryStream();
            workbook.SaveAs(stream);
            stream.Flush();

            return new FileContentResult(stream.ToArray(),
                "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            {
                FileDownloadName = $"products_{DateTime.UtcNow.ToShortDateString()}.xlsx"
            };
        }

        [HttpPost]
        public async Task<IActionResult> List([FromBody]DtQuery query)
        {
            return Json(await _itemService.GetAllItems(query));
        }
        
        public  IActionResult AddItem()
        {
           var types =  _itemService.GetAllTypes();
           var selectList = new SelectList(types, "Id", "Type");
           ViewBag.Types = selectList;
           return View();
        }
        
        [HttpPost]
        public async Task<IActionResult> AddItem(ItemViewModel itemDto)
        {
            var types = _itemService.GetAllTypes();
            var selectList = new SelectList(types, "Id", "Type");
            ViewBag.Types = selectList;
            if (!ModelState.IsValid) return View();
            var wwwRootPath = _webHostEnvironment.WebRootPath;
            await ImageMapper.MapImage(itemDto, wwwRootPath);
            var item = new Item
            {
                Name = itemDto.Name,
                Price = itemDto.Price,
                Description = itemDto.Description,
                ImageName = itemDto.ImageName,
                TypeId = itemDto.TypeId
            };
            await _itemService.CreateItem(item);
            return Redirect("/Item/Items");
        }
        [HttpGet("[controller]/{itemId}/[action]")]
        public async Task<IActionResult> EditItem([FromRoute]int itemId)
        {
            var item = await _itemService.GetItem(itemId);
            var itemViewModel = new ItemViewModel()
            {
                Id = item.Id,
                Name = item.Name,
                Price = item.Price,
                Description = item.Description,
                ImageName = item.ImageName,
                TypeId = item.TypeId
            };
            
            return View(itemViewModel);
        }
        [HttpPost]
        public async Task<IActionResult> EditItemPost(ItemViewModel itemDto)
        {
            if (!ModelState.IsValid) return View("EditItem");
            var wwwRootPath = _webHostEnvironment.WebRootPath;
            Item item;
            if (itemDto.ImageFile == null)
            {
                item = new Item
                {
                    Id = itemDto.Id,
                    Name = itemDto.Name,
                    Price = itemDto.Price,
                    Description = itemDto.Description,
                    TypeId = itemDto.TypeId
                };
                await _itemService.UpdateItem(item);
                return Redirect("/Item/Items");
            }
            await ImageMapper.MapImage(itemDto, wwwRootPath);
                
            item = new Item
            {
                Id = itemDto.Id,
                Name = itemDto.Name,
                Price = itemDto.Price,
                Description = itemDto.Description,
                ImageName = itemDto.ImageName,
                TypeId = itemDto.TypeId
            };
            await _itemService.UpdateItem(item);
           return Redirect("/Item/Items");

        }
        
        [HttpDelete("[controller]/{itemId}")]
        public async Task<IActionResult> DeleteItem([FromRoute]int itemId)
        {
            await _itemService.RemoveItem(itemId);
            return Ok();
        }
    }
}