﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MusicStore.Business.Interfaces;
using MusicStore.WebApp.Models;

namespace MusicStore.WebApp.Controllers
{
    [AllowAnonymous]
    public class VisualizationController : Controller
    {
        private readonly IVisualizationService _visualizationService;
        public VisualizationController(IVisualizationService visualizationService)
        {
            _visualizationService = visualizationService;
        }
        
        public async Task<IActionResult> Index()
        {
            var products = _visualizationService.GetAllRecalls();
            var labels = new List<string>(products.Count());
            var recallsCount = new List<int>(products.Count());
            
            labels.AddRange(products.Select(x => x.Name));
            foreach (var item in await products.ToListAsync())
            {
                recallsCount.Add(await _visualizationService.RecallsCount(item.Id));
            }
            var model = new VizualizationVIewModel()
            {
                Labels = labels,
                RecallsCount = recallsCount
            };
            
            return View(model);
        }
    }
}