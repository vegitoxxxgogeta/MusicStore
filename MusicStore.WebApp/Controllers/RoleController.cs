﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MusicStore.Business.Interfaces;
using MusicStore.Business.Responses;
using MusicStore.Data;
using MusicStore.WebApp.Models;

namespace MusicStore.WebApp.Controllers
{
    public class RoleController : Controller
    {
        RoleManager<IdentityRole> _roleManager;
        UserManager<ApplicationUser> _userManager;
        private readonly IUserService _userService;
       
        public RoleController(RoleManager<IdentityRole> roleManager, UserManager<ApplicationUser> userManager, IUserService userService)
        {
            _roleManager = roleManager;
            _userManager = userManager;
            _userService = userService;
        }
        public IActionResult Index() => View(_roleManager.Roles.ToList());
 
        public IActionResult Create() => View();
        
        [HttpPost]
        public async Task<IActionResult> Create(CreateRole role)
        {
            if (!ModelState.IsValid) return View();
            var result = await _roleManager.CreateAsync(new IdentityRole(role.Name));
            if (result.Succeeded)
            {
                return RedirectToAction("Index");
            }
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
            return View(role.Name);
        }
         
        [HttpPost]
        public async Task<IActionResult> Delete(string id)
        {
            var role = await _roleManager.FindByIdAsync(id);
            if (role != null)
            {
                var result = await _roleManager.DeleteAsync(role);
            }
            return RedirectToAction("Index");
        }
        
        public IActionResult UserList () => View();
        
        [HttpPost]
        public async Task<IActionResult> UserList([FromBody]DtQuery query)
        {
          
            var dbQuery = _userManager.Users.Where(x => x.Email != "admin@mail.ru");
            
            var lowerQ = query.Search?.ToLower();
            if (!string.IsNullOrWhiteSpace(lowerQ))
            {
                dbQuery = (lowerQ?.Split(' ')).Aggregate(dbQuery, (current, searchWord) =>
                    current.Where(f =>
                        f.Surname.ToLower().Contains(searchWord) ||
                        f.Name.ToLower().Contains(searchWord) ||
                        f.Email.ToString().ToLower().Contains(searchWord)
                    ));
            }

            // sorting
            if (!string.IsNullOrWhiteSpace(query.OrderColumn) && !string.IsNullOrWhiteSpace(query.OrderDir))
            {
                dbQuery = query.OrderColumn switch
                {
                    "surname" => (query.OrderDir.Equals("asc"))
                        ? dbQuery.OrderBy(o => o.Surname)
                        : dbQuery.OrderByDescending(o => o.Surname),
                    "name" => (query.OrderDir.Equals("asc"))
                        ? dbQuery.OrderBy(o => o.Name)
                        : dbQuery.OrderByDescending(o => o.Name),
                    "email" => (query.OrderDir.Equals("asc"))
                        ? dbQuery.OrderBy(o => o.Email)
                        : dbQuery.OrderByDescending(o => o.Email),
                    _ => (query.OrderDir.Equals("asc"))
                        ? dbQuery.OrderBy(o => o.Id)
                        : dbQuery.OrderByDescending(o => o.Id)
                };
            }
            
            // total count
            var totalItemCount = dbQuery.Count();

            // paging
            dbQuery = dbQuery.Skip((query.PageNumber - 1) * query.PageSize).Take(query.PageSize);
            
            var dbItems = await dbQuery.ToArrayAsync();
            
            return Json(new PagedResponse<ApplicationUser[]>(dbItems, totalItemCount, query.PageNumber, query.PageSize));
        } 
        [HttpGet("[controller]/{id}/[action]")]
        public async Task<IActionResult> EditRole(string id)
        {
         
            var user = await _userManager.FindByIdAsync(id);
            if (user == null) return NotFound();
            var userRoles = await _userManager.GetRolesAsync(user);
            var allRoles = _roleManager.Roles.ToList();
            var model = new ChangeRoleViewModel
            {
                Id = user.Id,
                UserEmail = user.Email,
                UserRoles = userRoles,
                AllRoles = allRoles
            };
            return View("Edit", model);

        }
        
        [HttpPost]
        public async Task<IActionResult> EditRolePost(string id, List<string> roles)
        {
          
            var user = await _userManager.FindByIdAsync(id);
            if (user == null) return NotFound();
            var userRoles = await _userManager.GetRolesAsync(user);
              
            var allRoles = _roleManager.Roles.ToList();
              
            var addedRoles = roles.Except(userRoles);
              
            var removedRoles = userRoles.Except(roles);
 
            await _userManager.AddToRolesAsync(user, addedRoles);
 
            await _userManager.RemoveFromRolesAsync(user, removedRoles);
 
            return RedirectToAction("UserList");

        }
        [HttpGet("[controller]/{id}/[action]")]
        public async Task<IActionResult> EditUser(string id)
        {
            var user = await _userManager.FindByIdAsync(id);
            if (user == null) return NotFound();
            var userViewModel = new UserViewModel()
            {
                Id = user.Id,
                Name = user.Name,
                Surname = user.Surname,
                Address = user.Address
            };
            return PartialView("UserDetails", userViewModel);
        }
        
        [HttpPost]
        public async Task<IActionResult> EditUserPost(ApplicationUser userDto)
        {
            var user = await _userManager.FindByIdAsync(userDto.Id);
            if (!ModelState.IsValid) return View("UserDetails");
            user.Name = userDto.Name;
            user.Surname = userDto.Surname;
            user.Address = userDto.Address;
            await _userManager.UpdateAsync(user);
            return Redirect("/Role/UserList");
        }
        
        [HttpGet("[controller]/{id}/{name}/[action]")]
        public  IActionResult RemoveUser(string id, string name)
        {
            var userViewModel = new UserViewModel()
            {
                Id = id,
                Name = name
            };
            return View("ConfirmDelete", userViewModel);
        }
        [HttpPost]
        public async Task<IActionResult> RemoveUserPost(UserViewModel userDto)
        {
            await _userService.RemoveUser(userDto.Id);
            return Redirect("/Role/UserList");
        }
        
        [HttpGet("[controller]/{id}/[action]")]
        public async Task<IActionResult> ChangePassword(string id)
        {
            var user = await _userManager.FindByIdAsync(id);
            var passwordModel = new ChangePasswordModel()
            {
                Id = user.Id
            };
            return PartialView(passwordModel);
        }
        public async Task<IActionResult> ChangeAndHashPass(ChangePasswordModel model)
        {
            ApplicationUser user = await _userManager.FindByIdAsync(model.Id);
            if (user == null)
            {
                return NotFound();
            }
            user.PasswordHash = _userManager.PasswordHasher.HashPassword(user, model.Password);
            var result = await _userManager.UpdateAsync(user);
            if (!result.Succeeded)
            {
                return BadRequest();
            }
            return Ok();
        }
        
        [HttpPost]
        public async Task<IActionResult> ChangePasswordPost(ChangePasswordModel model)
        {
            if (!ModelState.IsValid) return View("ChangePassword");
            await  ChangeAndHashPass(model);
            return Redirect("/Role/UserList");
        }
    }   
    }
