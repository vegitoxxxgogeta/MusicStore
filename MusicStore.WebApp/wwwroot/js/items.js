﻿let GetItemsController = function (translateTable) {

    let $deleteItemModal = $('#deleteItemModal');
    let $editItemModal = $('#editItemModal');
    let $deleteItemBtn = $('#deleteItemBtn');
    let $editItemBtn = $('#editItemBtn');

    function init() {
        const $tbl = $('#item-tbl').DataTable({
            language: {
                "paginate" : {
                    "previous" : translateTable.previous  ,
                    "next" : translateTable.next
                },
                "search": translateTable.search,
                "sLengthMenu" : "Zeige _MENU_ Einträge",
                "info" :`${translateTable.showing} _START_ ${translateTable.to} _END_ ${translateTable.of} _TOTAL_ ${translateTable.entries}`  
            },
            searching: true,
            "bLengthChange": false,
            searchDelay: 300,
            stateSave: true,
            processing: true,
            serverSide: true,
            ordering: true,
            order: [[ 1, "desc" ]],
            rowId: 'id',
            ajax: function(data, callback, settings) {
                dtAjaxHandler('/Item/List', data, callback)
            },
            columnDefs: [
                { targets: 0, title: "ID", data: "id" },
                { targets: 1, title: translateTable.name, data: "name" },
                { targets: 2, title: translateTable.description, data: "description" },
                { targets: 3, title: translateTable.price, data: "price" },
                { targets: 4, title: translateTable.type, data: "type.type" },


                {
                    targets: 5, title: "", data: "id", className: "text-center", orderable: false,
                    render: function ( itemId, type, row, meta ) {

                        return `<button  class="btn btn-info" data-action="showEditItemModal" id="showEditItemModal"><i class="bi bi-pencil"></i></button>`

                    }
                },
                {
                    targets: 6, title: "", data: "itemId", className: "text-center", orderable: false,
                    render: function ( itemId, type, row, meta ) {

                        return `<button data-action="showDeleteItemModal" type="button" class="btn btn-danger " title="Delete item"><i class="bi bi-trash"></i></button>`

                    }
                }
            ]
        });

        $tbl.on('click', 'button[data-action="showDeleteItemModal"]', function (ev) {
            let $row = $(ev.currentTarget).closest('tr');
            let itemId = $row.attr('id');

            $deleteItemModal.data('itemId', itemId)
            $deleteItemModal.find('.modal-body').text(`${translateTable.confirmDelete} ID '${itemId}'?`)
            $deleteItemModal.modal('show')
        })
        
        $tbl.on('click', 'button[data-action="showEditItemModal"]', function (ev) {
            let $row = $(ev.currentTarget).closest('tr');
            let itemId = $row.attr('id');
            $.get(`/Item/${itemId}/EditItem`, function(data){
                $editItemModal.html(data);
                $editItemModal.data('itemId', itemId)
                $editItemModal.modal('show')
            });
            
           
        })
        
        $('#deleteItemBtn').click(function () {
            let itemId = $deleteItemModal.data('itemId')

            loader.show();

            $.ajax({
                url: '/Item/' + itemId,
                type: 'DELETE',
                success: function () {
                    $tbl.ajax.reload();
                    $deleteItemModal.modal('hide');
                }
            })
        })
    }

    return {
        init
    }
}