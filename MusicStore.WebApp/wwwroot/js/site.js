﻿function showValidationSummaryErrors(containerSelector, errors) {
    const $container = $(containerSelector);

    const errorsHtml = errors.map((error) => {
        return `<li>${error}</li>`;
    })

    const html =
        `<div class="text-danger validation-summary-errors" data-valmsg-summary="true">
            <ul>${errorsHtml}</ul>
        </div>`

    $container.html(html).show();
}

function hideValidationSummaryErrors(containerSelector) {
    const $container = $(containerSelector);
    $container.empty().hide();
}
function updateCart(){
    $.get( "/Cart/GetItemsCount", function( data ) {
        $( "#cart" ).text( `(${data})`);
    });
}

const loader = {
    show() {
        $('.fullscreen-spinner').show();
    },
    hide() {
        $('.fullscreen-spinner').attr("style", 'display: none !important;');
    }
}

function dtAjaxHandler(url, data, callback) {
    $.ajax({
        url: url,
        type: 'POST',
        contentType: "application/json",
        dataType: "json",
        data: JSON.stringify(dtAjaxQueryToObj(data)),
        success: function (res) {
            callback({
                recordsTotal: res.metadata.totalItemCount,
                recordsFiltered: res.metadata.totalItemCount,
                data: res.data,
                draw: data.draw
            });
        }
    })
}

function dtAjaxQueryToObj(d) {
    let orderColumn = d.order != null && d.order.length > 0 ? d.order[0].column : null;
    return {
        draw: d.draw,
        pageNumber: d.length > 0 ? (d.start / d.length) + 1 : -1,
        pageSize: d.length,
        search: d.search != null ? d.search.value : null,
        orderColumn: orderColumn != null && d.columns != null && d.columns.length > 0 ? d.columns[orderColumn].data : null,
        orderDir: orderColumn != null ? d.order[0].dir : null
    };
}

function camelize(str) {
    return str.replace(/(?:^\w|[A-Z]|\b\w)/g, function(word, index) {
        return index === 0 ? word.toLowerCase() : word.toUpperCase();
    }).replace(/\s+/g, '');
}