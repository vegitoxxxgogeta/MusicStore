﻿let GetTypesController = function (translateTable) {

    let $deleteItemModal = $('#deleteItemModal');
    let $editItemTypeModal = $('#editItemTypeModal');
    let $deleteItemBtn = $('#deleteItemBtn');
    let $editItemBtn = $('#editItemBtn');

    function init() {
        const $tbl = $('#item-type-tbl').DataTable({
            language: {
                "paginate" : {
                    "previous" : translateTable.previous  ,
                    "next" : translateTable.next
                },
                "search": translateTable.search,
                "sLengthMenu" : "Zeige _MENU_ Einträge",
                "info" :`${translateTable.showing} _START_ ${translateTable.to} _END_ ${translateTable.of} _TOTAL_ ${translateTable.entries}`
            },
            searching: true,
            "bLengthChange": false,
            searchDelay: 300,
            stateSave: true,
            processing: true,
            serverSide: true,
            ordering: true,
            order: [[ 1, "desc" ]],
            rowId: 'id',
            ajax: function(data, callback, settings) {
                dtAjaxHandler('/ItemType/Types', data, callback)
            },
            columnDefs: [
                { targets: 0, title: "ID", data: "id" },
                { targets: 1, title: translateTable.name, data: "type" },
                
                {
                    targets: 2, title: "", data: "id", className: "text-center", orderable: false,
                    render: function ( typeId, type, row, meta ) {

                        return `<button  class="btn btn-info" data-action="showEditItemTypeModal" id="showEditItemTypeModal"><i class="bi bi-pencil"></i></button>`

                    }
                },
                {
                    targets: 3, title: "", data: "typeId", className: "text-center", orderable: false,
                    render: function ( typeId, type, row, meta ) {

                        return `<button data-action="showDeleteItemModal" type="button" class="btn btn-danger " title="Delete item"><i class="bi bi-trash"></i></button>`

                    }
                }
            ]
        });

        $tbl.on('click', 'button[data-action="showDeleteItemModal"]', function (ev) {
            let $row = $(ev.currentTarget).closest('tr');
            let typeId = $row.attr('id');

            $deleteItemModal.data('typeId', typeId)
            $deleteItemModal.find('.modal-body').text(`${translateTable.confirmDelete} ID '${typeId}'?`)
            $deleteItemModal.modal('show')
        })

        $tbl.on('click', 'button[data-action="showEditItemTypeModal"]', function (ev) {
            let $row = $(ev.currentTarget).closest('tr');
            let typeId = $row.attr('id');
            $.ajax({
                url: '/ItemType/' + typeId + '/EditType',
                type: 'Get',
                success: function (data) {
                    $('#editContent').html(data);
                    $editItemTypeModal.modal('show');
                }
            })
        })

        $('#deleteItemBtn').click(function () {
            let typeId = $deleteItemModal.data('typeId')

            loader.show();

            $.ajax({
                url: '/ItemType/' + typeId,
                type: 'DELETE',
                success: function () {
                    $tbl.ajax.reload();
                    $deleteItemModal.modal('hide');
                }
            })
        })
    }

    return {
        init
    }
}