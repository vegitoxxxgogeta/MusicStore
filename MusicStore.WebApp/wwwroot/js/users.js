﻿let GetRoleController = function (translateTable) {
    
    function init() {
        const $tbl = $('#user-tbl').DataTable({
            language: {
                "paginate" : {
                    "previous" : translateTable.previous  ,
                    "next" : translateTable.next
                },
                "search": translateTable.search,
                "sLengthMenu" : "Zeige _MENU_ Einträge",
                "info" :`${translateTable.showing} _START_ ${translateTable.to} _END_ ${translateTable.of} _TOTAL_ ${translateTable.entries}`
            },
            searching: true,
            "bLengthChange": false,
            searchDelay: 300,
            stateSave: true,
            processing: true,
            serverSide: true,
            ordering: true,
            order: [[ 1, "desc" ]],
            rowId: 'id',
            ajax: function(data, callback, settings) {
                dtAjaxHandler('/Role/UserList', data, callback)
            },
            columnDefs: [
                { targets: 0, title: "ID", data: "id" },
                { targets: 1, title: translateTable.name, data: "name" },
                { targets: 2, title: translateTable.surname, data: "surname" },
                { targets: 3, title: "Email", data: "email" },
                {
                    targets: 4, title: "", data: "id", className: "text-center", orderable: false,
                    render: function ( itemId, type, row, meta ) {

                        return `<button  class="btn btn-primary" data-action="showEditRoleModal" id="showEditItemModal"><i class="bi bi-person-lines-fill"></i></button>`

                    }
                },
                {
                    targets: 5, title: "", data: "id", className: "text-center", orderable: false,
                    render: function ( id, type, row, meta ) {

                        return `<button data-action="showEditUserModal" type="button" class="btn btn-info btn"><i class="bi bi-pencil"></i></button>`

                    }
                },
                {
                    targets: 6, title: "", data: "id", className: "text-center", orderable: false,
                    render: function ( id, type, row, meta ) {

                        return `<button data-action="showRemoveUserModal" type="button" class="btn btn-danger"><i class="bi bi-trash"></i></button>`

                    }
                },
                {
                    targets: 7, title: "", data: "id", className: "text-center", orderable: false,
                    render: function ( id, type, row, meta ) {

                        return `<button data-action="showChangePassModal" type="button" class="btn btn-dark"><i class="bi bi-key"></i></button>`

                    }
                }
            ]
        });
        $tbl.column(0).visible(false);
        $tbl.on('click', 'button[data-action="showEditUserModal"]', function (ev) {
            let $row = $(ev.currentTarget).closest('tr');
            let id = $row.attr('id');
            $.ajax({
                url: `/Role/${id}/EditUser`,
                type: 'Get',
                success: function (data) {
                    $('#dialogContent').html(data);
                    $('#modDialog').modal('show');
                }
            })
          
        })

        $tbl.on('click', 'button[data-action="showEditRoleModal"]', function (ev) {
            let $row = $(ev.currentTarget).closest('tr');
            let id = $row.attr('id');
            $.ajax({
                url: `/Role/${id}/EditRole`,
                type: 'Get',
                success: function (data) {
                    $('#dialogContent').html(data);
                    $('#modDialog').modal('show');
                }
            })


        })
        
        $tbl.on('click', 'button[data-action="showRemoveUserModal"]', function (ev) {
            let $row = $(ev.currentTarget).closest('tr');
            let id = $row.attr('id');
            let name = $row.find("td:eq(0)").text();
            $.ajax({
                url: `/Role/${id}/${name}/RemoveUser`,
                type: 'Get',
                success: function (data) {
                    $('#dialogContent').html(data);
                    $('#modDialog').modal('show');
                }
            })


        })
        
        $tbl.on('click', 'button[data-action="showChangePassModal"]', function (ev) {
            let $row = $(ev.currentTarget).closest('tr');
            let id = $row.attr('id');
            $.ajax({
                url: `/Role/${id}/ChangePassword`,
                type: 'Get',
                success: function (data) {
                    $('#dialogContent').html(data);
                    $('#modDialog').modal('show');
                }
            })


        })
    }

    return {
        init
    }
}