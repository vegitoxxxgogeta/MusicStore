﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MusicStore.WebApp.Resources.Models {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class UserViewModel_ru {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal UserViewModel_ru() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("MusicStore.WebApp.Resources.Models.UserViewModel.ru", typeof(UserViewModel_ru).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Поле не соответствует требованиям.
        /// </summary>
        internal static string Name_field_is_not_match_with_rules {
            get {
                return ResourceManager.GetString("Name field is not match with rules", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Поле не соответствует требованиям.
        /// </summary>
        internal static string Surname_field_is_not_match_with_rules {
            get {
                return ResourceManager.GetString("Surname field is not match with rules", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Поле Адрес обязательно.
        /// </summary>
        internal static string The_Address_field_is_must_be_required {
            get {
                return ResourceManager.GetString("The Address field is must be required", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Поле Имя обязательно.
        /// </summary>
        internal static string The_Name_field_is_must_be_required {
            get {
                return ResourceManager.GetString("The Name field is must be required", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Поле Фамилия обязательно.
        /// </summary>
        internal static string The_Surname_field_is_must_be_required {
            get {
                return ResourceManager.GetString("The Surname field is must be required", resourceCulture);
            }
        }
    }
}
