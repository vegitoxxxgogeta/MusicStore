﻿using System.IO;
using System.Threading.Tasks;
using MusicStore.WebApp.Models;

namespace MusicStore.WebApp.Helpers
{
    public class ImageMapper
    {
        public static async Task MapImage(ItemViewModel itemDto, string wwwRootPath)
        {
            try
            {
                var fileName = Path.GetFileNameWithoutExtension(itemDto.ImageFile.FileName);
                var extension = Path.GetExtension(itemDto.ImageFile.FileName);
                itemDto.ImageName = fileName + extension;
                var path = Path.Combine(wwwRootPath + "/Image/", fileName + ".jpg");
                await using (var fileStream = new FileStream(path, FileMode.Create))
                {
                    await itemDto.ImageFile.CopyToAsync(fileStream);
                }
            }
            catch
            {
                // ignored
            }
        }
    }
}