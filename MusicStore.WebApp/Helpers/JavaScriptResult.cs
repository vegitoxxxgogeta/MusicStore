﻿using Microsoft.AspNetCore.Mvc;

namespace MusicStore.WebApp.Helpers
{
    public class JavaScriptResult : ContentResult
    {
        public JavaScriptResult(string script)
        {
            this.Content = script;
            this.ContentType = "application/javascript";
        }
    }
}