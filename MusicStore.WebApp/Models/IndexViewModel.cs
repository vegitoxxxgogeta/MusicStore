﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;
using MusicStore.Data.Models;
using X.PagedList;

namespace MusicStore.WebApp.Models
{
    public class IndexViewModel
    {
        public IPagedList<Item> Items {get; set;}
        public List<Cart> Cart {get; set;}
        public PaginatedList<ItemType> Types {get; set;}
        public IPagedList<Order> Orders {get; set;}
        public IPagedList<UsersOrders> BootstrapUsersOrders {get; set;}
        public IPagedList<AnonymousOrders> BootstrapAnonymousOrders {get; set;}
        public PaginatedList<UsersOrders> UsersOrders {get; set;}
        public PaginatedList<AnonymousOrders> AnonymousOrders {get; set;}
        public List<int> ItemsQuantities { get; set; }
        
        
    }
}