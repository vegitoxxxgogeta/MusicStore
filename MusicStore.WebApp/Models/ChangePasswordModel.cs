using System.ComponentModel.DataAnnotations;

namespace MusicStore.WebApp.Models
{
    public class ChangePasswordModel
    {
        public string Id { get; set; }
        [Required(ErrorMessage = "Password field is required")]
        [RegularExpression(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[#$^+=!*()@%&]).{6,}$", ErrorMessage = "Password field is not match with rules")]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }
    }
}