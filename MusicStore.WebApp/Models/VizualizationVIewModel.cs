﻿using System.Collections.Generic;

namespace MusicStore.WebApp.Models
{
    public class VizualizationVIewModel
    {
        public List<string> Labels { get; set; }
        public List<int> RecallsCount { get; set; }
    }
}