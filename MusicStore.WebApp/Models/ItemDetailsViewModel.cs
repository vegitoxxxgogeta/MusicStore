﻿using System.Collections.Generic;

namespace MusicStore.WebApp.Models
{
    public class ItemDetailsViewModel
    {
        public ItemViewModel Item { get; set; }
        public List<RecallViewModel> Recall { get; set; }
    }
}