﻿using System.ComponentModel.DataAnnotations;

namespace MusicStore.WebApp.Models
{
    public class AnonymousViewModel
    {
         [EmailAddress]
         [Required(ErrorMessage = "EmailRequired")]
         public string Email { get; set; }
         
         [Required(ErrorMessage = "AddressRequired")]
         public string Address { get; set; }
        
    }
}