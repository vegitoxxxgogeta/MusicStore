﻿using MusicStore.Business.Requests;

namespace MusicStore.WebApp.Models
{
    public class DtQuery : PagedRequest
    {
        public int Draw { get; set; }     
    }
}