﻿using System.ComponentModel.DataAnnotations;

namespace MusicStore.WebApp.Models
{
    public class CreateRole
    {
        [Required(ErrorMessage = "Field required")]
        public string Name { get; set; }
    }
}