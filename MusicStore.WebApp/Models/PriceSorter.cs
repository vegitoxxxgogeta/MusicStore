﻿namespace MusicStore.WebApp.Models
{
    public class PriceSorter
    {
        public int Value { get; set; }
        public string SortName { get; set; }
    }
}