﻿using System;

namespace MusicStore.WebApp.Models
{
    public class RecallViewModel
    {
        public string Text { get; set; }
        public string User { get; set; }
        public int Rate { get; set; }
        public int ItemFkId { get; set; }
        public DateTime Date { get; set; }
    }
}